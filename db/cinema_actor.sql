-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cinema
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actor`
--

DROP TABLE IF EXISTS `actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `actor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `actor_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actor`
--

LOCK TABLES `actor` WRITE;
/*!40000 ALTER TABLE `actor` DISABLE KEYS */;
INSERT INTO `actor` VALUES (16,'Brad Pitt',46,'5e75c6490c1cafa54841e3a1723b117d.jpeg'),(17,'Kate Winslet',45,'800c35203c3ecbe1cdea0eaa1959d547.png'),(18,'Joseph Gordon-Levitt',39,'87c14db3f6dc0fe3cf48b82ec5bbeda0.jpeg'),(19,'Ellen Page',33,'1eb749495eb59f5958b2ac3c2098cf9b.jpeg'),(20,'Tom Hardy',43,'cf6dcf13566274efa1de4647c4ea118a.jpeg'),(21,'Tim Robbins',62,'2b29522b161dc1c5f1c1d6c32eca5d14.jpeg'),(22,'Morgan Freeman',83,'8a18c41c8d601a630fefbb80d01a8b16.jpeg'),(23,'Bob Gunton',75,'9cc30a996671ee5381ffd813add779d7.jpeg'),(24,'William Sadler',70,'d63017583ec8673bdaf14dabf9ef8e81.jpeg'),(25,'Joaquin Phoenix',46,'158f57d38c02bc941d8608e5bf468346.jpeg'),(26,'Robert De Niro',73,'9e91874e4c8f5efcb04479b1aee586b7.jpeg'),(27,'Zazie Beetz',29,'b8f3c605d5d9620d3443b1b60c332c94.jpeg'),(28,'Frances Conroy',67,'e36a78cbd6674d80fec57c0200635547.jpeg'),(29,'Billy Zane',54,'19173f049f344463ba03f3829f901760.jpeg'),(30,'Victor Garber',71,'71334a199f079fa9c31f27bf97c971b0.jpeg');
/*!40000 ALTER TABLE `actor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-26 22:58:17
