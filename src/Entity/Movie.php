<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 */
class Movie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $MovieName;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $Director;

    /**
     * @ORM\Column(type="integer")
     */
    private $ReleaseYear;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\Image()
	 */
	private $Image;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Actor", mappedBy="movie")
     */
    private $actors;

    public function __construct()
    {
        $this->actors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMovieName(): ?string
    {
        return $this->MovieName;
    }

    public function setMovieName(string $MovieName): self
    {
        $this->MovieName = $MovieName;

        return $this;
    }

    public function getDirector(): ?string
    {
        return $this->Director;
    }

    public function setDirector(string $Director): self
    {
        $this->Director = $Director;

        return $this;
    }

	public function getImage()
	{
		return $this->Image;
	}

	public function getImageFile()
	{
    	$file = new File();

	}

	public function setImage($Image)
	{
		$this->Image = $Image;

		return $this;
	}

    public function getReleaseYear(): ?int
    {
        return $this->ReleaseYear;
    }

    public function setReleaseYear(int $ReleaseYear): self
    {
        $this->ReleaseYear = $ReleaseYear;

        return $this;
    }

    /**
     * @return Collection|Actor[]
     */
    public function getActors(): Collection
    {
        return $this->actors;
    }

    public function addActor(Actor $actor): self
    {
    	$this->actors[] = $actor;

        return $this;
    }

    public function removeActor(Actor $actor): self
    {
        if ($this->actors->contains($actor)) {
            $this->actors->removeElement($actor);
            $actor->removeMovie($this);
        }

        return $this;
    }
}
