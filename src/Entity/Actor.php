<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActorRepository")
 */
class Actor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Movie", inversedBy="actors", cascade={"persist"})
     * @ORM\JoinTable(
     *     name = "actor_movie",
     *     joinColumns={@ORM\JoinColumn(name="actor_id",referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="movie_id",referencedColumnName="id")}
     * )
     */
    private $movie;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $ActorName;

    /**
     * @ORM\Column(type="integer")
     */
    private $Age;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\Image()
	 */
	private $Image;

    public function __construct()
    {
        $this->movie = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Movie[]
     */
    public function getMovie(): Collection
    {
        return $this->movie;
    }

    public function addMovie(Movie $movie): self
    {
    	$this->movie[] = $movie;

        return $this;
    }

    public function removeMovie(Movie $movie): self
    {
        if ($this->movie->contains($movie)) {
            $this->movie->removeElement($movie);
        }

        return $this;
    }

    public function getActorName(): ?string
    {
        return $this->ActorName;
    }

    public function setActorName(string $ActorName): self
    {
        $this->ActorName = $ActorName;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->Age;
    }

    public function setAge(int $Age): self
    {
        $this->Age = $Age;

        return $this;
    }

	public function getImage()
	{
		return $this->Image;
	}

	public function setImage($Image)
	{
		$this->Image = $Image;

		return $this;
	}
}
