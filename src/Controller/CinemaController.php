<?php
    namespace App\Controller;

    use App\Entity\Actor;
    use App\Entity\Movie;
    use Symfony\Bridge\Doctrine\Form\Type\EntityType;
    use Symfony\Component\Filesystem\Filesystem;
    use Symfony\Component\HttpFoundation\File\Exception\FileException;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Routing\Annotation\Route;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
    use Symfony\Component\Form\Extension\Core\Type\FileType;

    class CinemaController extends AbstractController{

        /**
         * @Route("/",methods={"GET"}, name="index")
         */
        public function index(){

            return $this->render('cinemas/index.html.twig');
        }

	    /**
	     * @Route("/movie",methods={"GET"}, name="movie_list")
	     */
	    public function MovieList(){

		    $movies = $this->getDoctrine()->getRepository(Movie::class)->findAll();
		    return $this->render('cinemas/movielist.html.twig', array('movies' => $movies));
	    }

	    /**
	     * @Route("/actor",methods={"GET"}, name="actor_list")
	     */
	    public function ActorList(){

		    $actors = $this->getDoctrine()->getRepository(Actor::class)->findAll();
		    return $this->render('cinemas/actorlist.html.twig', array('actors' => $actors));
	    }

        /**
         * @Route("/movie/new", name="new_movie", methods={"GET", "POST"})
         */
        public function NewMovie(Request $request){
        	$movie = new Movie();
	        $actors = $this->getDoctrine()->getRepository(Actor::class)->findAll();

        	$form = $this->createFormBuilder($movie)
		        ->add('MovieName', TextType::class, array(
		        	'attr' => array('class' => 'form-control')))
		        ->add('Director', TextType::class, array(
		        	'attr' => array('class' => 'form-control')))
		        ->add('ReleaseYear', TextType::class, array(
			        'attr' => array('class' => 'form-control')))
		        ->add('Image', FileType::class, array(
			        'attr' => array('class' => 'form-control')))
		        ->add('actors', EntityType::class, array(
		        	'class' => Actor::class,
			        'choices' => $actors,
			        'choice_label' => 'ActorName',
			        'multiple' => true,
			        'required' => false
		        ))
		        ->add('Save', SubmitType::class, array(
		        	'label' => 'Create',
			        'attr' => array('class' => 'btn btn-primary m-3')))
		        ->getForm();

        	$form->handleRequest($request);

        	if ($form->isSubmitted() && $form->isValid())
	        {
	        	$movie = $form->getData();

		        /**
		         * @var UploadedFile $file
		         */
		        $file = $form->get('Image')->getData();
		        $fileName = md5(uniqid()).'.'.$file->guessExtension();

		        try {
			        $file->move('images', $fileName);
		        }catch (FileException $e)
		        {
		        	echo $e->getMessage();
		        }
		        foreach ($movie->getActors() as $actor)
		        {
			        $movie->addActor($actor);
			        $actor->addMovie($movie);
		        }

		        $movie->setImage($fileName);
	        	$entityManager = $this->getDoctrine()->getManager();
	        	$entityManager->persist($movie);
	        	$entityManager->flush();

		        return $this->redirectToRoute('movie_list');
	        }

			return $this->render('cinemas/newmovie.html.twig', array(
				'form' => $form->createView()));
        }

	    /**
	     * @Route("/movie/edit/{id}", name="edit_movie", methods={"GET", "POST"})
	     */
	    public function EditMovie(Request $request, $id){
		    $movie = new Movie();
		    $movie = $this->getDoctrine()->getRepository(
			    Movie::class)->find($id);

		    $imageFileName = $movie->getImage();

		    $actors = $this->getDoctrine()->getRepository(Actor::class)->findAll();

		    $form = $this->createFormBuilder($movie)
			    ->add('MovieName', TextType::class, array(
				    'attr' => array('class' => 'form-control')))
			    ->add('Director', TextType::class, array(
				    'attr' => array('class' => 'form-control')))
			    ->add('ReleaseYear', TextType::class, array(
				    'attr' => array('class' => 'form-control')))
			    ->add('Image', FileType::class, array(
				    'attr' => array('class' => 'form-control'),
				    'data_class' => null,
				    'required' => false))
			    ->add('Save', SubmitType::class, array(
				    'label' => 'Update',
				    'attr' => array('class' => 'btn btn-primary m-3')))
			    ->getForm();

		    $form->handleRequest($request);

		    if ($form->isSubmitted() && $form->isValid())
		    {
			    $file = $form->get('Image')->getData();

			    if ($file === null)
			    {
			    	$movie->setImage($imageFileName);
			    }
			    else
			    {
				    $file = $form->get('Image')->getData();
				    $fileName = md5(uniqid()).'.'.$file->guessExtension();

				    try {
					    $file->move('images', $fileName);
				    }catch (FileException $e)
				    {
					    echo $e->getMessage();
				    }

				    $movie->setImage($fileName);
			    }

			    $entityManager = $this->getDoctrine()->getManager();
			    $entityManager->flush();

			    return $this->redirectToRoute('movie_list');
		    }

		    return $this->render('cinemas/editmovie.html.twig', array(
			    'form' => $form->createView()));
	    }

	    /**
	     * @Route("/actor/new", name="new_actor", methods={"GET", "POST"})
	     */
	    public function NewActor(Request $requestActor){
		    $actor = new Actor();
		    $movies = $this->getDoctrine()->getRepository(Movie::class)->findAll();

		    $formActor = $this->createFormBuilder($actor)
			    ->add('ActorName', TextType::class, array(
				    'attr' => array('class' => 'form-control')))
			    ->add('Age', TextType::class, array(
				    'attr' => array('class' => 'form-control')))
			    ->add('Image', FileType::class, array(
				    'attr' => array('class' => 'form-control')))
			    ->add('movie', EntityType::class, array(
				    'class' => Movie::class,
				    'choices' => $movies,
				    'choice_label' => 'MovieName',
				    'multiple' => true
			    ))
			    ->add('Save', SubmitType::class, array(
				    'label' => 'Create',
				    'attr' => array('class' => 'btn btn-primary m-3')))
			    ->getForm();

		    $formActor->handleRequest($requestActor);

		    if ($formActor->isSubmitted() && $formActor->isValid())
		    {
			    $actor = $formActor->getData();

			    /**
			     * @var UploadedFile $file
			     */
			    $file = $formActor->get('Image')->getData();
			    $fileName = md5(uniqid()).'.'.$file->guessExtension();

			    try {
				    $file->move('images', $fileName);
			    }catch (FileException $e)
			    {
				    echo $e->getMessage();
			    }

			    foreach ($actor->getMovie() as $movie)
			    {
				    $movie->addActor($actor);
			    }

			    $actor->setImage($fileName);
			    $entityManager = $this->getDoctrine()->getManager();
			    $entityManager->persist($actor);
			    $entityManager->flush();

			    return $this->redirectToRoute('actor_list');
		    }

		    return $this->render('cinemas/newactor.html.twig', array(
			    'form' => $formActor->createView()));
	    }

	    /**
	     * @Route("/actor/edit/{id}", name="edit_actor", methods={"GET", "POST"})
	     */
	    public function EditActor(Request $requestActor, $id){
		    $actor = new Actor();
		    $actor = $this->getDoctrine()->getRepository(
			    Actor::class)->find($id);

		    $imageFileName = $actor->getImage();

		    $formActor = $this->createFormBuilder($actor)
			    ->add('ActorName', TextType::class, array(
				    'attr' => array('class' => 'form-control')))
			    ->add('Age', TextType::class, array(
				    'attr' => array('class' => 'form-control')))
			    ->add('Image', FileType::class, array(
				    'attr' => array('class' => 'form-control'),
				    'data_class' => null,
				    'required' => false))
			    ->add('Save', SubmitType::class, array(
				    'label' => 'Update',
				    'attr' => array('class' => 'btn btn-primary m-3')))
			    ->getForm();

		    $formActor->handleRequest($requestActor);

		    if ($formActor->isSubmitted() && $formActor->isValid())
		    {
			    $file = $formActor->get('Image')->getData();

			    if ($file === null)
			    {
				    $actor->setImage($imageFileName);
			    }
			    else
			    {
				    $file = $formActor->get('Image')->getData();
				    $fileName = md5(uniqid()).'.'.$file->guessExtension();

				    try {
					    $file->move('images', $fileName);
				    }catch (FileException $e)
				    {
					    echo $e->getMessage();
				    }

				    $actor->setImage($fileName);
			    }

			    $entityManager = $this->getDoctrine()->getManager();
			    $entityManager->flush();

			    return $this->redirectToRoute('actor_list');
		    }

		    return $this->render('cinemas/editactor.html.twig', array(
			    'form' => $formActor->createView()));
	    }

	    /**
	     * @Route("/movie/{id}", methods={"GET"}, name="movie_show")
	     */
	    public function ShowMovie($id){
		    $movie = $this->getDoctrine()->getRepository(
			    Movie::class)->find($id);
		    return $this->render('cinemas/showmovie.html.twig',
			    array('movie' => $movie));
	    }

	    /**
	     * @Route("/movie/delete/{id}", methods={"GET","DELETE"})
	     * @return Response
	     */
	    public function DeleteMovie(Request $request, $id)
	    {
		    $movie = $this->getDoctrine()->getRepository(
			    Movie::class)->find($id);


		    foreach ($movie->getActors() as $actor){
		    	$movie->removeActor($actor);
		    }

		    $entityManager = $this->getDoctrine()->getManager();

		    $entityManager->remove($movie);
		    $entityManager->flush();

		    return $this->redirectToRoute('movie_list');
	    }

	    /**
	     * @Route("/actor/delete/{id}", methods={"GET","DELETE"})
	     */
	    public function DeleteActor(Request $request, $id)
	    {
		    $actor = $this->getDoctrine()->getRepository(
			    Actor::class)->find($id);


		    foreach ($actor->getMovie() as $movie){
		        $actor->removeMovie($movie);
		    }

		    $entityManager = $this->getDoctrine()->getManager();

		    $entityManager->remove($actor);
		    $entityManager->flush();

		    return $this->redirectToRoute('actor_list');
	    }

	    /**
	     * @Route("/actor/{id}", methods={"GET"}, name="actor_show")
	     */
	    public function ShowActor($id){
		    $actor = $this->getDoctrine()->getRepository(
			    Actor::class)->find($id);
		    return $this->render('cinemas/showactor.html.twig',array('actor' => $actor));
	    }
    }

